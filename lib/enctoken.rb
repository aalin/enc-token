require_relative './enctoken/version'
require 'openssl'
require 'yaml'

class EncToken
  DEFAULT_EXPIRES_IN_SECONDS = 5 * 60

  class DecryptError < StandardError ; end
  class TokenExpiredError < StandardError ; end

  def initialize(passphrase)
    @key = Digest::SHA256.new.tap { |digest|
      digest.update(passphrase)
    }.digest
  end

  def encrypt(data, expires_in = DEFAULT_EXPIRES_IN_SECONDS)
    cipher = make_cipher(:encrypt)
    cipher.iv = iv = cipher.random_iv

    now = Time.now

    token_data = {
      expires_at: now + expires_in,
      time: now,
      data: data
    }

    (iv + cipher.update(YAML.dump(token_data)) + cipher.final).unpack("H*").first
  end

  def decrypt(token)
    cipher = make_cipher(:decrypt)
    cipher.iv, token_str = [token.to_s].pack("H*").unpack("a16 a*")

    token = YAML.load(cipher.update(token_str) + cipher.final)

    if token[:expires_at] < Time.now
      raise TokenExpiredError, "Token expired at #{ token[:expires_at] }"
    end

    token[:data]
  rescue OpenSSL::Cipher::CipherError => e
    raise DecryptError, e.message
  end

  private

  def make_cipher(method)
    raise ArgumentError, "encrypt or decrypt?" unless [:encrypt, :decrypt].include?(method)

    cipher = OpenSSL::Cipher::Cipher.new('AES-256-CBC')
    cipher.send(method)
    cipher.key = @key
    cipher
  end
end
