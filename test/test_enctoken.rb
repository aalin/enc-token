require 'minitest/autorun'

class TestEncToken < Minitest::Test
  def setup
    @enctoken = EncToken.new('my secret key')
  end

  def test_encrypt_decrypt
    data = { foo: 'bar', baz: 'waz' }
    token = @enctoken.encrypt(data)
    assert_equal @enctoken.decrypt(token), data
  end

  def test_bad_passphrase
    data = { foo: 'bar', baz: 'waz' }
    token = @enctoken.encrypt(data)

    assert_raises EncToken::DecryptError do
      EncToken.new('other key').decrypt(token)
    end
  end

  def test_expired_token
    data = { foo: 'bar', baz: 'waz' }
    token = @enctoken.encrypt(data, 0)

    error = assert_raises EncToken::TokenExpiredError do
      @enctoken.decrypt(token)
    end

    assert_match /^Token expired at \d+/, error.message
  end
end
