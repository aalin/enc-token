# EncToken

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'enctoken'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install enctoken

## Usage

```ruby
require 'enctoken'

enctoken = EncToken.new("my secret key")
token = enctoken.encrypt(foo: 'bar', baz: 'waz')
puts token #=>
enctoken = EncToken.new("my secret key")
token = enctoken.encrypt(foo: 'bar', baz: 'waz')
p enctoken.decrypt(token) #=> { foo: 'bar', baz: 'waz' }
```

By default, tokens are valid for 5 minutes.
You can pass in how many seconds you want your tokens to expire after, in the encrypt call, example:

```ruby
token = enctoken.encrypt({ foo: 'bar', baz: 'waz' }, 0)
enctoken.decrypt(token) #=> TokenExpiredError, Token expired at 2015-03-14 16:35:57 +0100
```

## Contributing

1. Fork it ( https://github.com/[my-github-username]/enctoken/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
